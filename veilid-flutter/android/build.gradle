buildscript {
    ext.kotlin_version = '1.6.10'
    repositories {
        google()
        mavenCentral()
    }

    dependencies {
        classpath 'com.android.tools.build:gradle:7.2.0'
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
    }
}

plugins {
    id "org.mozilla.rust-android-gradle.rust-android" version "0.9.3"
}

group 'com.veilid.veilid'
version '1.0-SNAPSHOT'

rootProject.allprojects {
    repositories {
        google()
        mavenCentral()
    }
}

apply plugin: 'com.android.library'
apply plugin: 'kotlin-android'

android {
    compileSdkVersion 31

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = '1.8'
    }

    sourceSets {
        main.java.srcDirs += 'src/main/kotlin'
    }

    defaultConfig {
        minSdkVersion 24
        targetSdkVersion 31
        versionCode 1
        versionName "1.0"
        
        ndk {
            abiFilters 'armeabi-v7a', 'arm64-v8a', 'x86', 'x86_64'
        }

        // Required to copy libc++_shared.so
        externalNativeBuild {
            cmake {
                arguments "-DANDROID_STL=c++_shared"
                targets "cpplink"
            }
        }
    }

    ndkVersion '25.1.8937393'
 
    // Required to copy libc++_shared.so
    externalNativeBuild {
        cmake {
            path file('CMakeLists.txt')
        }
    }
}

dependencies {
    implementation "androidx.security:security-crypto:1.1.0-alpha03"
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version"
}

apply plugin: 'org.mozilla.rust-android-gradle.rust-android'

cargo {
    module  = "../rust"
    libname = "veilid_flutter"
    targets = ["arm", "arm64", "x86", "x86_64"]
    targetDirectory = "../../target"
    pythonCommand = "python3"
    profile = gradle.startParameter.taskNames.any{it.toLowerCase().contains("debug")} ? "debug" : "release"
}

afterEvaluate {
    // The `cargoBuild` task isn't available until after evaluation.
    android.libraryVariants.all { variant ->
        def productFlavor = ""
        variant.productFlavors.each {
            productFlavor += "${it.name.capitalize()}"
        }
        def buildType = "${variant.buildType.name.capitalize()}"
        tasks["generate${productFlavor}${buildType}Assets"].dependsOn(tasks["cargoBuild"])
    }
}
